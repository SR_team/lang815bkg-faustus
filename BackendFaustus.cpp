#include "BackendFaustus.h"
#include <fstream>
#include <iostream>
#include <lang815/Config.h>

BackendFaustus::BackendFaustus( Lang815::Config *config ) : Lang815::IBackend( config ) {}

bool BackendFaustus::switchColor( unsigned long group ) {
	std::ofstream red( "/sys/devices/platform/faustus/kbbl/kbbl_red" );
	std::ofstream green( "/sys/devices/platform/faustus/kbbl/kbbl_green" );
	std::ofstream blue( "/sys/devices/platform/faustus/kbbl/kbbl_blue" );
	std::ofstream flags( "/sys/devices/platform/faustus/kbbl/kbbl_flags" );
	std::ofstream set( "/sys/devices/platform/faustus/kbbl/kbbl_set" );

	if ( !red.is_open() || !green.is_open() || !blue.is_open() || !flags.is_open() || !set.is_open() ) {
		std::cerr << "Failed to open files in /sys/devices/platform/faustus/kbbl/" << std::endl;
		return false;
	}

	auto color = config->getColor( group );
	red << std::hex << (int)color.red << std::endl;
	green << std::hex << (int)color.green << std::endl;
	blue << std::hex << (int)color.blue << std::endl;
	flags << std::hex << 0x2a << std::endl;
	set << std::hex << 0x02 << std::endl;

	red.close();
	green.close();
	blue.close();
	flags.close();
	set.close();

	return true;
}

BackendFaustus *instance( Lang815::Config *config ) {
	static BackendFaustus self( config );
	return &self;
}
