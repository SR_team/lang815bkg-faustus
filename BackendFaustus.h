#ifndef BACKENDFAUSTUS_H
#define BACKENDFAUSTUS_H

#include "Lang815bkg-faustus_global.h"
#include <lang815/IBackend.h>

class LANG815BKGFAUSTUS_EXPORT BackendFaustus : public Lang815::IBackend {
public:
	BackendFaustus( Lang815::Config *config );

	virtual bool switchColor( unsigned long group );
};

extern "C" LANG815BKGFAUSTUS_EXPORT BackendFaustus *instance( Lang815::Config *config );

#endif // BACKENDFAUSTUS_H
