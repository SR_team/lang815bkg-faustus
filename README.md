This library is backend for [Lang815](https://gitlab.com/SR_team/lang815) to add support Asus TUF Gaming laptops

## Install

### ArchLinux

```bash
git clone https://aur.archlinux.org/lang815bkg-faustus.git
cd lang815bkg-faustus
makepkg -siCc
```

If you use AUR helpers you can use special command, like one of this:

```bash
yay -S lang815bkg-faustus # with yay
pacaur -S lang815bkg-faustus # with pacaur
trizen -S lang815bkg-faustus # with trizen
```

### Manual

Before manual installation you need **[build](#Build)** project.

* copy file `libLang815bkg-faustus.so` to `/usr/lib/`

### After installation

Enable this backend:

1. Change backend to `libLang815bkg-faustus.so` in **Lang815** config (default location `~/config/Lang815.json`)
2. Restart service - `systemctl --user restart org.lang815.switch`

## Build

#### Depends

* [Lang815](https://gitlab.com/SR_team/lang815)

```bash
cmake -DCMAKE_BUILD_TYPE=MinSizeRel .
make
```
